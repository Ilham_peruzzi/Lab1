from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Ilham Peruzzi' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,6,9) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
response={}
def index(request):
	if 'user_login' in request.session:
		response['author'] = request.session['user_login']
		response['kode_identitas'] = request.session['kode_identitas']
		response['connected'] = True
		response['name'] = mhs_name
		response['age'] = calculate_age(birth_date.year)
		return render(request, 'index_lab1.html', response)
	else:
		response['connected'] = False
		response['name'] = 'Muhammad Ilham Peruzzi'
		html = 'lab_9/session/login.html'
		return render(request, html, response)
    

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
