from django.shortcuts import render

# Create your views here.
response={}
def index(request):
	if 'user_login' in request.session:
		response['author'] = request.session['user_login']
		response['kode_identitas'] = request.session['kode_identitas']
		response['connected'] = True
		response['name'] = 'Muhammad Ilham Peruzzi'
		html = 'lab_6/lab_6.html'
		return render(request, html, response)
	else:
		response['connected'] = False
		response['name'] = 'Muhammad Ilham Peruzzi'
		html = 'lab_9/session/login.html'
		return render(request, html, response)
	