from django.shortcuts import render, redirect
from .models import Diary
from datetime import datetime
import pytz
import json
# Create your views here.
diary_dict = {}
response={}
def index(request):
    if 'user_login' in request.session:
        response['author'] = request.session['user_login']
        response['kode_identitas'] = request.session['kode_identitas']
        response['connected'] = True
        response['name'] = 'Muhammad Ilham Peruzzi'
        diary_dict = Diary.objects.all().values()
        response['diary_dict'] = convert_queryset_into_json(diary_dict)
        return render(request, 'to_do_list.html', response)
    else:
        response['connected'] = False
        response['name'] = 'Muhammad Ilham Peruzzi'
        html = 'lab_9/session/login.html'
        return render(request, html, response)
    

def add_activity(request):
    if request.method == 'POST':
        try:
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
            return redirect('/lab-3/')
        except:
            return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
