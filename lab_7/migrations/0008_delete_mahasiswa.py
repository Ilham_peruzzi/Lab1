# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-14 01:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0007_auto_20171113_1730'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Mahasiswa',
        ),
    ]
