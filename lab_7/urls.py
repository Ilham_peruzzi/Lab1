from django.conf.urls import url
from .views import index, add_friend, validate_npm,delete_friend,friend_list,data_friend,get_friend_detail,get_data_user_view
urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-friend/$', add_friend, name='add-friend'),
	url(r'^validate-npm/$', validate_npm, name='validate-npm'),
	url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
	url(r'^get-friend-list/delete-friend/(?P<friend_id>[0-9]+)/$',delete_friend, name='delete-friend'),
	url(r'^data-friend/$', data_friend, name='data-friend'),
	url(r'^get-friend-list/get-friend-detail/(?P<friend_id>[0-9]+)/$', get_friend_detail, name='get-friend-detail'),
	url(r'^get-data-user/$', get_data_user_view, name='get-data-user'),
]
