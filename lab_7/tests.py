from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,get_friend_detail,data_friend,friend_list,add_friend,delete_friend,validate_npm,model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
import requests
import urllib
import os
import environ
from lab_9.csui_helper import get_access_token,get_mahasiswa_list

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
# Create your tests here.
class Lab7UnitTest(TestCase):
	
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab7_using_right_template(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_9/session/login.html')
		session = self.client.session
		session['user_login'] = 'test'
		session['kode_identitas'] = '123'
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		session['access_token'] = get_access_token(self.username,self.password)
		session.save()
		response = self.client.get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_7/lab_7.html')

	def test_paginator(self):
		data = ['a','b','c','d','e','f']
		paginator = Paginator(data, 2)
		page1 = paginator.page(1)
		page2 = paginator.page(2)
		page3 = paginator.page(3)
		self.assertEqual(page1.object_list, list(data[0:2]))
		self.assertEqual(page2.object_list, list(data[2:4]))
		self.assertEqual(page3.object_list, list(data[4:6]))
		
	def test_can_add_friend(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_9/session/login.html')
		session = self.client.session
		session['user_login'] = 'test'
		session['kode_identitas'] = '123'
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		session['access_token'] = get_access_token(self.username,self.password)
		session.save()
		response_post = self.client.post('/lab-7/get-data-user/', {'npm': '1606823475'})


	def test_get_mahasiswa_detail(self):
		friend = Friend.objects.create(friend_name="Anonymous", npm="123456789")
		response = Client().post('/lab-7/get-friend-list/get-friend-detail/' + str(friend.id) + '/')
		self.assertEqual(response.status_code, 200)
		session = self.client.session
		session['user_login'] = 'test'
		session['kode_identitas'] = '123'
		session.save()
		response = self.client.get('/lab-7/get-friend-list/get-friend-detail/' + str(friend.id) + '/')
		html_response = response.content.decode('utf8')
		self.assertIn('Anonymous', html_response)
		self.assertIn('123456789', html_response)
			

	def test_data_friend_url_is_exist(self):
		response = Client().get('/lab-7/data-friend/')
		self.assertEqual(response.status_code, 200)


	def test_friend_list_page(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertTemplateUsed(response, 'lab_9/session/login.html')
		session = self.client.session
		session['user_login'] = 'test'
		session['kode_identitas'] = '123'
		session.save()
		friend = Friend.objects.create(friend_name="Anonymous", npm="123456789")
		response = Client().post('/lab-7/add-friend/', {'name':"Anonymous2", 'npm':"123", 'alamat':"", 'kode_pos':" ", 'kota_lahir':" ",'tanggal_lahir':''})
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'lab_7/daftar_teman.html')


	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Pikachu", npm="12211221")
		response = Client().post('/lab-7/get-friend-list/delete-friend/' + str(friend.id) + '/')
		self.assertEqual(response.status_code, 200)
		self.assertNotIn(friend, Friend.objects.all())

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})

	'''def test_username_dan_password_salah(self):
		username = "ilham"
		password = "rahasia"
		csui_helper = CSUIhelper()
		with self.assertRaises(Exception) as context:
			csui_helper.instance.get_access_token(username, password)
		self.assertIn("ilham", str(context.exception))

	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])'''










