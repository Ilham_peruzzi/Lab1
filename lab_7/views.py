from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Friend
from lab_9.csui_helper import get_mahasiswa_list, get_data_user, get_client_id
import os
import json
import requests
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


API_MAHASISWA = "https://api.cs.ui.ac.id/siakngcs/mahasiswa/"
response = {}
def index(request):
    if 'user_login' in request.session:
        response['author'] = request.session['user_login']
        response['kode_identitas'] = request.session['kode_identitas']
        response['connected'] = True
        response['name'] = 'Muhammad Ilham Peruzzi'
        access_token = request.session['access_token']
        mahasiswa_list = get_mahasiswa_list(access_token)
        friend_list = Friend.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(mahasiswa_list, 10)
        mahasiswa_list = paginator.page(page)
        response['mahasiswa_list'] = mahasiswa_list
        response['friend_list'] = friend_list
        html = 'lab_7/lab_7.html'
        return render(request, html, response)
    else:
        response['connected'] = False
        response['name'] = 'Muhammad Ilham Peruzzi'
        html = 'lab_9/session/login.html'
        return render(request, html, response)

@csrf_exempt
def get_data_user_view(request):
    if request.method == "POST":
        npm = request.POST['npm']
        access_token = request.session['access_token']
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_MAHASISWA+npm, params=parameters)
        data = {'json':response.json()}
        return JsonResponse(data)

def get_friend_detail(request, friend_id):
    if 'user_login' not in request.session.keys():
        response['connected'] = False
        response['name'] = 'Muhammad Ilham Peruzzi'
        html = 'lab_9/session/login.html'
        return render(request, html, response)
    ## end of sol
    friend = Friend.objects.get(id=friend_id)
    response['friend'] = friend;
    html = 'lab_7/detail.html'
    return render(request, html, response)


@csrf_exempt
def data_friend(request):
	friend_list = Friend.objects.all()
	SomeModel_json = serializers.serialize("json", friend_list)
	data = {"json": SomeModel_json}
	return JsonResponse(data)

def friend_list(request):
    if 'user_login' not in request.session.keys():
        response['connected'] = False
        response['name'] = 'Muhammad Ilham Peruzzi'
        html = 'lab_9/session/login.html'
        return render(request, html, response)
    ## end of sol
    response['connected'] = True
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        alamat = request.POST['alamat']
        kode_pos = request.POST['kode_pos']
        kota_lahir = request.POST['kota_lahir']
        tanggal_lahir = request.POST['tanggal_lahir']
        friend_list = Friend.objects.all()
        is_taken = Friend.objects.filter(npm__iexact=npm).exists()
        if not is_taken:
        	friend = Friend(friend_name=name, npm=npm, alamat=alamat, kode_pos=kode_pos,kota_lahir=kota_lahir,tanggal_lahir=tanggal_lahir)
        	friend.save()
        	data = model_to_dict(friend)
        	return HttpResponse(data)

@csrf_exempt
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    data = {
        'status': 'ok'
    }
    return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists() #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

