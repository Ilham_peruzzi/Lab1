from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
# Create your views here.
response = {}
about_me = ["Male","Awesome","Handsome","Attractive","Smart","Independent","Sweet","Wise","Appreciative"]
def index(request):
	if 'user_login' in request.session:
		response['author'] = request.session['user_login']
		response['kode_identitas'] = request.session['kode_identitas']
		response['connected'] = True
		response['name'] = 'Muhammad Ilham Peruzzi'
		response['content'] = landing_page_content
		response['about_me'] = about_me
		response['message_form'] = Message_Form
		html = 'lab_4/lab_4.html'
		return render(request, html, response)
	else:
		response['connected'] = False
		response['name'] = 'Muhammad Ilham Peruzzi'
		html = 'lab_9/session/login.html'
		return render(request, html, response)
	


def message_post(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
		response['message'] = request.POST['message']
		message = Message(name=response['name'],email =response['email'],message = response['message'])
		message.save()
		html = 'lab_4/form_result.html'
		return render(request, html, response)	
	else:
		return HttpResponseRedirect('/lab-4/')




def message_table(request):
	## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
    	response['connected'] = False
    	response['name'] = 'Muhammad Ilham Peruzzi'
    	html = 'lab_9/session/login.html'
    	return render(request, html, response)
    ## end of sol
    response['author'] = request.session['user_login']
    response['kode_identitas'] = request.session['kode_identitas']
    response['connected'] = True
    response['name'] = 'Muhammad Ilham Peruzzi'
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request,html,response)