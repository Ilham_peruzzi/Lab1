from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
        'blank' : 'This field is required.'
        
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email',required=False, widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(label='Message',widget=forms.Textarea(attrs=attrs), required=True)
