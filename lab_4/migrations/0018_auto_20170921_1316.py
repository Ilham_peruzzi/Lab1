# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-21 06:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0017_auto_20170921_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
