from django.db import models
from django.utils import timezone
# Create your models here.
class Todo(models.Model):
	def convertTZ():
		return timezone.now() + timezone.timedelta(hours=7)
	title = models.CharField(max_length=27)
	description = models.TextField()
	created_date = models.DateTimeField(default=convertTZ)
