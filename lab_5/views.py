from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo
from django.contrib import messages

# Create your views here.
response = {}
def index(request):
    if 'user_login' in request.session:
        response['author'] = request.session['user_login']
        response['kode_identitas'] = request.session['kode_identitas']
        response['connected'] = True
        response['name'] = 'Muhammad Ilham Peruzzi'
        todo = Todo.objects.all()
        response['todo'] = todo
        html = 'lab_5/lab_5.html'
        response['todo_form'] = Todo_Form
        return render(request, html, response)
    else:
        response['connected'] = False
        response['name'] = 'Muhammad Ilham Peruzzi'
        html = 'lab_9/session/login.html'
        return render(request, html, response)    
    

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        todo = Todo(title=response['title'],description=response['description'])
        todo.save()
        messages.success(request, 'Berhasil menambahkan Todo')
        return HttpResponseRedirect('/lab-5/')
    else:
        return HttpResponseRedirect('/lab-5/')

def delete_todo(request, id):
    instance = Todo.objects.get(id=id)
    instance.delete()
    return HttpResponseRedirect('/lab-5/')