from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'I am now study at Faculty of Computer Science, University of Indonesia, since 2016. I like to read books and listening the music. Now I am learning about how to make a website in Web Design and Programming class and this is my first website.'
response={}
def index(request):
	if 'user_login' in request.session:
		response['author'] = request.session['user_login']
		response['kode_identitas'] = request.session['kode_identitas']
		response['connected'] = True
		response['name'] = 'Muhammad Ilham Peruzzi'
		response['name']=mhs_name
		response['content']= landing_page_content
		return render(request, 'index_lab2.html', response)
	else:
		response['connected'] = False
		response['name'] = 'Muhammad Ilham Peruzzi'
		html = 'lab_9/session/login.html'
		return render(request, html, response)
    